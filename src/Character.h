/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Character
#define _Character

#include "sader/BaseCharacter.h"

enum Direction { UP, DOWN };

class Character : public sader::BaseCharacter
{
    protected:
    int m_score;
    public:
    void Init( int playerNumber );
    void Move( Direction dir );
    void IncrementScore() { m_score++; }
    float Score() { return m_score; }
};

#endif
