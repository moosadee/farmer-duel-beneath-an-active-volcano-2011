/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Hud
#define _Hud

#include "sader/BaseCharacter.h"

enum HudItem { TITLESCREEN, WIN_PLAYER_LABEL, WIN_PLAYER_NUMBER, WIN_LABEL,
                PLAYER_1_SCORE, PLAYER_2_SCORE };

class Hud : public sader::BaseCharacter
{
    public:
        Hud();
        void Init( HudItem type );
        void NumberSet( int val );
        void NumberInc();
    private:
};

#endif
