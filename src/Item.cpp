/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Item.h"

void Item::Init()
{
    ResetCoordinates();

    m_coord.w = m_coord.h = m_coord.d = 32;

    m_collision.w = m_collision.h = m_collision.d = 32;
    m_collision.x = m_collision.y = m_collision.z = 0;

    m_film.x = 0;
    m_film.y = 0;
    m_film.z = 0;
    m_film.w = 32;
    m_film.h = 32;
    m_film.d = 0;

    forceInc = 0.5;

    maxY = 108;
    minY = 16;

    collisionTimer = 0;
}

void Item::ResetCoordinates()
{
    ResetCoordinates( 1 );
}

void Item::ResetCoordinates( int playerThatScored )
{

    velX = 0;
    velY = 0;

    if ( playerThatScored == 1 )
    {
        velZ = 2.0;
    }
    else
    {
        velZ = -2.0;
    }

    m_coord.x = 320;
    m_coord.y = 16;
    m_coord.z = 276;
}

void Item::Move()
{
    m_coord.x += velX;
    m_coord.y += velY;
    m_coord.z += velZ;

    TempPhysicsHax();
}

void Item::TempPhysicsHax()
{
    if ( m_coord.x > 610 || m_coord.x < 0 )
    {
        velZ = -velZ;
        velX = -velX;
    }

    velY -= 0.5;

    if ( ( m_coord.y > maxY && velY > 0 ) ||
            ( m_coord.y < minY && velY < 0 ) )
    {
        velY = -velY;
    }

    if ( collisionTimer > 0 )
    {
        collisionTimer -= 1;
    }
}

void Item::ChangeDirection( float x, float z )
{
    float zMid = m_coord.z + (m_coord.d/2);

    if ( zMid < z )
    {
        velX += forceInc;
    }
    else if ( zMid > z )
    {
        velX -= forceInc;
    }

    velZ = -velZ;
    velY = -velY;

    if ( velZ < 0 ) { velZ -= forceInc; }
    else { velZ += forceInc; }
    if ( velY < 0 ) { velY -= 1; }
    else { velY += 1; }
}

