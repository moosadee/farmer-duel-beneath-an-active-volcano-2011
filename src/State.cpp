/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "State.h"

void MenuState::Init()
{
    LoadAssets();

    titleScreen.Init( TITLESCREEN );
    titleScreen.SetTexture( textureManager.GetAsset( "titlescreen" ) );
}

void MenuState::LoadAssets()
{
    std::list<std::string> textureAssets;
    textureAssets.push_back( "titlescreen.jpg" );

    textureManager.LoadAssets( textureAssets );
}

int MenuState::MainLoop( sader::Window* app )
{
    std::cout<<"Menu State"<<std::endl;

    input.ResetInputCooldown();
    closeState = false;

    input.SetWindow( app );

    while ( !closeState )
    {
        app->GetEvent();

        if ( input.GetKey( sf::Key::F4 ) )       { return EXIT; }
        if ( input.GetKey( sf::Key::Return ) )   { return GAME; }

        input.Update();

        // TODO: Clean
        // Draw
        TempRender( app );
    }

    return EXIT;
}

void MenuState::TempRender( sader::Window* app )
{
    app->window.SetActive();
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    glTranslatef( -1024/2, -768/2, -520 );

    titleScreen.Draw( sader::XY );

    app->window.Display();
}

void GameState::Init()
{
    LoadAssets();

    player1.Init( 1 );
    player1.SetTexture( textureManager.GetAsset( "players" ) );

    player2.Init( 2 );
    player2.SetTexture( textureManager.GetAsset( "players" ) );

    pong.Init();
    pong.SetTexture( textureManager.GetAsset( "items" ) );
    pong.MaxY( player1.CollisionRectangle().h );

    player1Score.Init( PLAYER_1_SCORE );
    player1Score.SetTexture( textureManager.GetAsset( "text-numbers" ) );

    player2Score.Init( PLAYER_2_SCORE );
    player2Score.SetTexture( textureManager.GetAsset( "text-numbers" ) );

    labelWins.Init( WIN_LABEL );
    labelWins.SetTexture( textureManager.GetAsset( "text-wins" ) );

    labelPlayer.Init( WIN_PLAYER_LABEL );
    labelPlayer.SetTexture( textureManager.GetAsset( "text-player" ) );

    labelNumber.Init( WIN_PLAYER_NUMBER );
    labelNumber.SetTexture( textureManager.GetAsset( "text-numbers" ) );

    cam.Init( -200, -190, -220, 25, 109, 0, 10, 2 );

    player1Boundary.SetCoordinates( 0, 0, -65 );
    player1Boundary.SetCollisionRegion( 0, -50, 0, 610, 200, 64 );

    player2Boundary.SetCoordinates( 0, 0, 615 );
    player2Boundary.SetCollisionRegion( 0, -50, 0, 610, 200, 64 );

    debug = false;

    m_playerWins = 0;
}

void GameState::LoadAssets()
{
    std::list<std::string> textureAssets;
    textureAssets.push_back( "tiles.png" );
    textureAssets.push_back( "items.png" );
    textureAssets.push_back( "players.png" );
    textureAssets.push_back( "text-player.png" );
    textureAssets.push_back( "text-numbers.png" );
    textureAssets.push_back( "text-wins.png" );
    std::list<std::string> mapAssets;
    mapAssets.push_back( "farm.poo" );

    textureManager.LoadAssets( textureAssets );
    mapManager.LoadAssets( mapAssets, textureManager.GetAsset( "tiles" ) );
}

int GameState::MainLoop( sader::Window* app )
{
    std::cout<<"Game Loop"<<std::endl;

    input.ResetInputCooldown();
    closeState = false;
    pause = true;

    input.SetWindow( app );

    while ( !closeState )
    {
        app->GetEvent();
        HandleInput();

        input.Update();

        if ( !pause && m_playerWins <= 0 )
        {
            pong.Move();

            // Collision tests
            if ( pong.CollisionTimer() <= 0 && pong.CollidingWith( &player1 ) )
            {
                pong.ChangeDirection( player1.X(), player1.Z() );
                pong.StartCollisionTimer();
            }
            if ( pong.CollisionTimer() <= 0 && pong.CollidingWith( &player2 ) )
            {
                pong.ChangeDirection( player2.X(), player2.Z() );
                pong.StartCollisionTimer();
            }
            if ( pong.CollidingWith( &player1Boundary ) )
            {
                player2.IncrementScore();
                player2Score.NumberInc();
                std::cout<<"Score: Player 1: "<<player1.Score()<<", Player 2: "<<player2.Score()<<std::endl;
                pong.ResetCoordinates( 2 );
            }
            if ( pong.CollidingWith( &player2Boundary ) )
            {
                player1.IncrementScore();
                player1Score.NumberInc();
                std::cout<<"Score: Player 1: "<<player1.Score()<<", Player 2: "<<player2.Score()<<std::endl;
                pong.ResetCoordinates( 1 );
            }

            if ( player1.Score() - player2.Score() > 3 )
            {
                labelNumber.NumberSet( 1 );
                m_playerWins = 1;
            }
            else if ( player2.Score() - player1.Score() > 3 )
            {
                labelNumber.NumberSet( 2 );
                m_playerWins = 2;
            }
        }
        TempRender( app );
    }
    return MENU;
}

void GameState::TempRender( sader::Window* app )
{
    app->window.SetActive();
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    glTranslatef( cam.TransX(), cam.TransY(), cam.TransZ() );

    player1Score.Draw( sader::XY );
    player2Score.Draw( sader::XY );

    if ( m_playerWins > 0 )
    {
        labelWins.Draw( sader::XY );
        labelPlayer.Draw( sader::XY );
        labelNumber.Draw( sader::XY );
    }

    glRotatef( cam.RotX(), 1.0f, 0.0f, 0.0f );
    glRotatef( cam.RotY(), 0.0f, 1.0f, 0.0f );
    glRotatef( cam.RotZ(), 0.0f, 0.0f, 1.0f );

    mapManager.GetAsset(0).Draw();

    player1.DrawShadow();
    player2.DrawShadow();
    pong.DrawShadow();

    player1.Draw( sader::YZ );
    player2.Draw( sader::YZ );
    pong.Draw( sader::XYYZ );

    if ( debug )
    {
        player1.DrawBoundingBox( 1.0f, 1.0f, 0.0f );
        player2.DrawBoundingBox( 1.0f, 0.0f, 1.0f );
        pong.DrawBoundingBox( 0.0f, 1.0f, 1.0f );

        player1Boundary.DrawBoundingBox( 1.0f, 1.0f, 0.0f );
        player2Boundary.DrawBoundingBox( 1.0f, 0.0f, 1.0f );
    }

    app->window.Display();
}

void GameState::HandleInput()
{
    input.Update();

    // Fix up next version
    if ( input.GetKey( sf::Key::F4 ) )          { closeState = true; }
    if ( input.GetKey( sf::Key::Return )
        || input.GetKey( sf::Key::Escape ) )    { pause = !pause; input.ResetInputCooldown(); }
    if ( input.GetKey( sf::Key::F6 ) &&
            input.InputCooldown() <= 0 )
    {
        debug = !debug;
        input.ResetInputCooldown();
        std::cout<<"Debug Mode "<<debug<<std::endl;
    }

    if ( !pause && m_playerWins <= 0 )
    {
        // Player 1 movement
        if ( input.GetKey( sf::Key::Up ) )      { player2.Move( UP ); }
        if ( input.GetKey( sf::Key::Down ) )    { player2.Move( DOWN ); }
        // Player 2 movement
        if ( input.GetKey( sf::Key::W ) )       { player1.Move( UP ); }
        if ( input.GetKey( sf::Key::S ) )       { player1.Move( DOWN ); }

        if ( debug )
        {
            if ( input.GetKey( sf::Key::Return ) )
            {
                std::cout<<"COORDINATES"<<std::endl;
                std::cout<<"Player 1"<<std::endl;
                player1.OutputCoordinates( 0, 0 );
                std::cout<<"Player 2"<<std::endl;
                player2.OutputCoordinates( 0, 0 );
            }

            if ( input.GetKey( sf::Key::Num1 ) )    { cam.TransX( 1 ); }
            if ( input.GetKey( sf::Key::Num2 ) )    { cam.TransX( -1 ); }
            if ( input.GetKey( sf::Key::Num3 ) )    { cam.TransY( 1 ); }
            if ( input.GetKey( sf::Key::Num4 ) )    { cam.TransY( -1 ); }
            if ( input.GetKey( sf::Key::Num5 ) )    { cam.TransZ( 1 ); }
            if ( input.GetKey( sf::Key::Num6 ) )    { cam.TransZ( -1 ); }

            if ( input.GetKey( sf::Key::Num7 ) )    { cam.RotX( 1 ); }
            if ( input.GetKey( sf::Key::Num8 ) )    { cam.RotX( -1 ); }
            if ( input.GetKey( sf::Key::Num9 ) )    { cam.RotY( 1 ); }
            if ( input.GetKey( sf::Key::Num0 ) )    { cam.RotY( -1 ); }
            if ( input.GetKey( sf::Key::Equal ) )   { cam.RotZ( 1 ); }
            if ( input.GetKey( sf::Key::Back ) )    { cam.RotZ( -1 ); }
        }
    }
}
