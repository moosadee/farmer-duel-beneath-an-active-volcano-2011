/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/

/*
2011-07-23: The code is kind of messy. When I began
running out of time, I began just writing stuff
to work, rather than to be pretty.  Both Sader and
Farmer Duel will have to be updated in the near future
to clean them up so that they're more separate
and so I can use the Sader framework in the future.

Questions? Comments?  Feel free to rip my
code apart, so long as you have legimiate criticisms.
--Rachel
*/

#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <iostream>

#include "sader/System.h"
#include "sader/StateManager.h"

#include "State.h"

int main()
{
    std::cout<<"FARMER DUEL beneath an ACTIVE VOLCANO"<<std::endl;
    std::cout<<"Moosader.com, (c) Rachel J. Morris, 2011"<<std::endl;
    std::cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"<<std::endl;

    sader::System system;
    sader::StateManager stateManager;

    GameState gameState;
    MenuState menuState;

    stateManager.SetRenderWindow( &system.app );

    int gameStateIndex = stateManager.AddState( &gameState );
    int menuStateIndex = stateManager.AddState( &menuState );
    stateManager.InitStates();

    int status = MENU;

    while ( status != EXIT )
    {
        std::cout<<"Active state "<<status<<std::endl;
        // Run game state. If the game loop for this state ends,
        // it will return another state, or EXIT.

        if ( status == MENU )
        {
            stateManager.SetActiveState( menuStateIndex );
        }
        else if ( status == GAME )
        {
            gameState.Init();
            stateManager.SetActiveState( gameStateIndex );
        }

        status = stateManager.MainLoop();
    }

    std::cout<<"\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<std::endl;
    std::cout<<"Thanks for playing... bye!"<<std::endl;

    return system.GetStatus();
}

/*
To Do:

x Write game loop
x Add system input (quit, pause)
x Draw textured polygons
x Load in level maps
Add in lighting?
Add background
x Add camera
x Add characters
x Add character movement
Adjust camera movement based on player positions (zoom in/out)
x Add items
x Add item/character collision base class)
x Pong ball item
Barn building items
Render trees
Add HUD elements
Add round countdown timer
Add menu system
*/




