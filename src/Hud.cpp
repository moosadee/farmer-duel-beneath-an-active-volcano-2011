/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Hud.h"

Hud::Hud()
{
    m_coord.x = m_coord.y = m_coord.z = 0;
    m_coord.w = m_coord.h = m_coord.d = 64;
}

void Hud::Init( HudItem type )
{
    if ( type == TITLESCREEN )
    {
        m_coord.x = 0;
        m_coord.y = 0;
        m_coord.z = 0;
        m_coord.w = 1024;
        m_coord.h = 768;
        m_coord.d = 2;

        m_film.x = 0;
        m_film.y = 0;
        m_film.w = 1024;
        m_film.h = 768;
    }
    else if ( type == WIN_PLAYER_LABEL )
    {
        m_coord.x = 25;
        m_coord.y = 200;
        m_coord.z = 0;

        m_coord.w = 256/2;
        m_coord.h = 64/2;
        m_coord.d = 2;

        m_film.x = 0;
        m_film.y = 0;
        m_film.w = 256;
        m_film.h = 64;
    }
    else if ( type == WIN_PLAYER_NUMBER )
    {
        m_coord.x = 175;
        m_coord.y = 200;
        m_coord.z = 0;

        m_coord.w = 64/2;
        m_coord.h = 64/2;
        m_coord.d = 2;

        m_film.x = 64;
        m_film.y = 0;
        m_film.w = 64;
        m_film.h = 64;
    }
    else if ( type == WIN_LABEL )
    {
        m_coord.x = 250;
        m_coord.y = 200;
        m_coord.z = 0;

        m_coord.w = 203/2;
        m_coord.h = 64/2;
        m_coord.d = 2;

        m_film.x = 0;
        m_film.y = 0;
        m_film.w = 203;
        m_film.h = 64;
    }
    else if ( type == PLAYER_1_SCORE )
    {
        m_coord.x = -10;
        m_coord.y = 375;
        m_coord.z = 0;

        m_coord.w = 32;
        m_coord.h = 32;
        m_coord.d = 2;

        m_film.x = 0;
        m_film.y = 0;
        m_film.w = 64;
        m_film.h = 64;
    }
    else if ( type == PLAYER_2_SCORE )
    {
        m_coord.x = 375;
        m_coord.y = 375;
        m_coord.z = 0;

        m_coord.w = 32;
        m_coord.h = 32;
        m_coord.d = 2;

        m_film.x = 0;
        m_film.y = 0;
        m_film.w = 64;
        m_film.h = 64;
    }
}

void Hud::NumberSet( int val )
{
    m_film.x = val * 64;
}

void Hud::NumberInc()
{
    if ( m_film.x + 64 < 640 )
    {
        m_film.x += 64;
    }
    else
    {
        m_film.x = 0;
    }
}
