/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Item
#define _Item

#include "sader/BaseCharacter.h"

class Item : public sader::BaseCharacter
{
    private:
        float velX, velY, velZ;
        float forceInc;
        float minY, maxY;
        // Temp fix for collision issues
        float collisionTimer;
        void TempPhysicsHax();
    public:
        void Init();
        void Move();
        void ChangeDirection( float x, float z );
        float CollisionTimer() { return collisionTimer; }
        void StartCollisionTimer() { collisionTimer = 10; }
        void ResetCoordinates();
        void ResetCoordinates( int playerThatScored );
        void MinY( float val ) { minY = val; }
        void MaxY( float val ) { maxY = val; }
};

#endif

