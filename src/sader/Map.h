/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/

#ifndef _Sader_Map
#define _Sader_Map

#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include <fstream>

#include "Asset.h"
#include "Texture.h"
#include "ManagerTemplate.h"
#include "Geometry.h"

namespace sader
{

class Map : public Asset
{
    private:
        std::list<Geometry> geometryList;
    public:
        Map();
        void Init();
        void Draw();
        bool Load( std::string& filepath );
        void SetGeometryTexture( Texture& tileset );
};

}

#endif
