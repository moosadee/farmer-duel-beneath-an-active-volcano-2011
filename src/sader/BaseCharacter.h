/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_Base_Character
#define _Sader_Base_Character

#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include "System.h"
#include "Geometry.h"

namespace sader
{

class BaseCharacter : public Geometry
{
    protected:
        float moveSpeed;
    public:
        void Move();
        bool CollidingWith( BaseCharacter* obj2 );
};

}

#endif

