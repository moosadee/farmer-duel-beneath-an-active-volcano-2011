/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_Geometry
#define _Sader_Geometry

#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include <iostream>

#include "Texture.h"
#include "Rect3D.h"

namespace sader
{

enum DrawPlane { XY, YZ, XYYZ, CUBE };

class Geometry
{
    protected:
        Rect3D m_coord; // item coordinates & dimensions
        Rect3D m_film;  // coordinates & dimensions on filmstrip
        Rect3D m_collision; // coordinates & dimensions of bounding box
        Texture* m_texture;

        virtual void DrawCube();
        virtual void DrawXY();
        virtual void DrawYZ();
        virtual void DrawXYYZ();

    public:
        Geometry();
        virtual void Init();
        virtual void SetCoordinates( float x, float y, float z );
        virtual void SetDimensions( float w, float h, float d );
        virtual void SetTexture( Texture& texture );
        virtual void Draw( DrawPlane drawPlane = CUBE );
        virtual void DrawBoundingBox( float r, float g, float b );
        virtual void DrawShadow();

        Rect3D& CollisionRectangle() { return m_collision; }

        // Clean this shit up!
        void FilmstripX( float val ) { m_film.x = val; }
        float FilmstripX() { return m_film.x; }
        void FilmstripY( float val ) { m_film.y = val; }
        float FilmstripY() { return m_film.y; }

        double FX1( float f ) { return (double)m_film.x / (double)f; }
        double FY1( float f ) { return (double)m_film.y / (double)f; }
        double FX2( float f ) { return (double)m_film.x / (double)f + (double)m_film.w / (double)f; }
        double FY2( float f ) { return (double)m_film.y / (double)f + (double)m_film.h / (double)f; }

        void X( float val ) { m_coord.x = val; }
        float X() { return m_coord.x; }
        void Y( float val ) { m_coord.y = val; }
        float Y() { return m_coord.y; }
        void Z( float val ) { m_coord.z = val; }
        float Z() { return m_coord.z; }

        void W( float val ) { m_coord.w = val; }
        float W() { return m_coord.w; }
        void H( float val ) { m_coord.h = val; }
        float H() { return m_coord.h; }
        void D( float val ) { m_coord.d = val; }
        float D() { return m_coord.d; }

        float X1() { return m_coord.x; }
        float Y1() { return m_coord.y; }
        float Z1() { return m_coord.z; }
        float X2() { return m_coord.x + m_coord.w; }
        float Y2() { return m_coord.y + m_coord.h; }
        float Z2() { return m_coord.z + m_coord.d; }

        float ColX1();
        float ColX2();
        float ColY1();
        float ColY2();
        float ColZ1();
        float ColZ2();
        void SetCollisionRegion( float x, float y, float z, float w, float h, float d );

        void OutputCoordinates( float fw, float fh )
        {
            std::cout<<"Coordinates"<<std::endl;
            std::cout<<"Coord: ("<<m_coord.x<<", "<<m_coord.y<<", "<<m_coord.z<<")\tDim: "
                <<m_coord.w<<"x"<<m_coord.h<<"x"<<m_coord.d<<std::endl;
            std::cout<<"Filmstrip"<<std::endl;
            std::cout<<FX1(fw)<<" - "<<FX2(fw)<<", "<<FY1(fh)<<" - "<<FY2(fh)<<std::endl;
            std::cout<<"------------------------------------------------"<<std::endl;
        }
};

}

#endif
