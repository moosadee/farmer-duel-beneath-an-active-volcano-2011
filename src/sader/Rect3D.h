#ifndef _Rect3D
#define _Rect3D

namespace sader
{

struct Rect3D
{
    float x, y, z;
    float w, h, d;

    void SetCoordinates( float x, float y, float z )
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    void SetDimensions( float w, float h, float d )
    {
        this->w = w;
        this->h = h;
        this->d = d;
    }
};

}

#endif
