/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/

#ifndef _Sader_Enumerations
#define _Sader_Enumerations

namespace sader
{

/**
* A wrapper for the types of assets there are
*/
enum AssetType { GRAPHIC, MAP };

/**
* A wrapper for the program's status, such as "0" for no errors.
*/
enum ProgramStatus {
                      ITSALLGOOD = 0
                   };

}

#endif
