/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Input.h"

namespace sader
{
    Input::Input()
    {
        // Set up key dictionary
        // ToDO: Rename
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::Right,         PLAYER_1_MOVE_UP ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::Left,       PLAYER_1_MOVE_DOWN ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::Space,      PLAYER_1_ATTACK ) );

        //m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::W,          PLAYER_2_MOVE_UP ) );
        //m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::S,          PLAYER_2_MOVE_DOWN ) );
        //m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::D,          PLAYER_2_ATTACK ) );

        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::F4,        STATE_EXIT ) );

        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::A,         CAMERA_X_PLUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::D,         CAMERA_X_MINUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::S,         CAMERA_Y_PLUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::W,         CAMERA_Y_MINUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::Q,         CAMERA_Z_PLUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::Z,         CAMERA_Z_MINUS ) );

        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::J,         CAMERA_X_ROT_PLUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::L,         CAMERA_X_ROT_MINUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::K,         CAMERA_Y_ROT_PLUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::I,         CAMERA_Y_ROT_MINUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::U,         CAMERA_Z_ROT_PLUS ) );
        m_controlDictionary.insert( std::pair<sf::Key::Code, KeyAction>( sf::Key::N,         CAMERA_Z_ROT_MINUS ) );
    }

    void Input::ResetInputCooldown()
    {
        tempKeyCooldown = 10;
    }

    void Input::SetWindow( Window* app )
    {
        this->app = app;
    }

    KeyAction Input::GetKeyAction( sf::Key::Code code )
    {
        return m_controlDictionary[ code ];
    }

    bool Input::GetKey( sf::Key::Code code )
    {
        if ( tempKeyCooldown <= 0 )
        {
            return app->window.GetInput().IsKeyDown( code );
        }
        return false;
    }

    void Input::Update()
    {
        if ( tempKeyCooldown > 0 )
        {
            tempKeyCooldown--;
        }
    }

// ToDo: Implement system later
    bool* Input::HandleInput()
    {
//        for ( std::map<sf::Key::Code, KeyAction>::iterator keyAction = m_controlDictionary.begin();
//            keyAction != m_controlDictionary.end(); keyAction++ )
//        {
//            actions[ (int)keyAction->second ] =
//                ( app->window.GetInput().IsKeyDown( keyAction->first ) ) ? true : false;
//        }

        return actions;
    }
}
