/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_Input
#define _Sader_input

#include <SFML/Window.hpp>

#include <iostream>
#include <map>
#include <vector>

#include "Window.h"
#include "KeyActionEnums.h"

// This doesn't belong in sader; should go in the game itself.

namespace sader
{
    class Input
    {
        private:
            // Binds key codes to actions (enum)
            std::map<sf::Key::Code, KeyAction> m_controlDictionary;
            bool actions[TOTAL_KEY_ACTIONS];
            Window* app;
            float tempKeyCooldown;
        public:
            Input();
            bool* HandleInput();
            bool GetKey( sf::Key::Code code );
            KeyAction GetKeyAction( sf::Key::Code code );
            void SetWindow( Window* app );
            void Update();
            void ResetInputCooldown();
            float InputCooldown() { return tempKeyCooldown; }
    };
}

#endif
