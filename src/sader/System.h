/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/

// TODO: Rename to something less ambiguous

#ifndef _Sader_System
#define _Sader_System

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <fstream>
#include <iostream>

#include "Enumerations.h"
#include "Window.h"

namespace sader
{

class System
{
  private:
    ProgramStatus m_status;
    bool m_fullscreen;
    int m_screenWidth;
    int m_screenHeight;
    int m_bpp;
  public:
    Window app;

    System();
    void Init();
    void GetEvent();

    ProgramStatus GetStatus();
};

}

#endif

