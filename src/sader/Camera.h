/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_Camera
#define _Sader_Camera

#include "System.h"

namespace sader
{

class Camera
{
    private:
        float m_xRotation, m_yRotation, m_zRotation;
        float m_xTransform, m_yTransform, m_zTransform;
        float m_TransformSpeed, m_RotationSpeed;
    public:
        Camera();
        void HandleInput( System& system );

        float RotX() { return m_xRotation; }
        float RotY() { return m_yRotation; }
        float RotZ() { return m_zRotation; }
        float TransX() { return m_xTransform; }
        float TransY() { return m_yTransform; }
        float TransZ() { return m_zTransform; }
        void CameraToPoint( float x, float y );
        void CameraToPoint( float x, float y, float z );
        void RotX( int multiplier );
        void RotY( int multiplier );
        void RotZ( int multiplier );
        void TransX( int multiplier );
        void TransY( int multiplier );
        void TransZ( int multiplier );

        void Prep();

        void Init( float mx, float my, float mz, float rx, float ry, float rz, float s, float rs );
};

}

#endif
