/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Map.h"

namespace sader
{

Map::Map()
{
}

void Map::Init()
{
}

void Map::SetGeometryTexture( Texture& tileset )
{
    for ( std::list<Geometry>::iterator tile = geometryList.begin();
            tile != geometryList.end(); ++tile )
    {
        tile->SetTexture( tileset );
    }
}

// ToDo: set pointer to texture for tileset, don't pass in entire manager each time.
void Map::Draw()
{
    // Draw potatoes (or not)
    for ( std::list<Geometry>::iterator tile = geometryList.begin(); tile != geometryList.end(); tile++ )
    {
        tile->Draw( CUBE );
    }
}

bool Map::Load( std::string& filename )
{
    // Get filename
    for ( unsigned int i=0; i<filename.size(); i++ )
    {
        if ( filename[i] == '.' )
        {
            m_name = filename.substr( 0, i );
            break;
        }
    }

    // Load file
    std::string filepath = "content/maps/" + filename;
    std::cout<<"Load map "<<filepath<<"...";

    std::ifstream infile( filepath.c_str() );

    if ( infile.bad() )
    {
        std::cout<<"Failed!"<<std::endl;
        return false;
    }
    else
    {
        std::cout<<"Success!"<<std::endl;
    }

    std::string command;
    float value;

    Geometry tile;
    while ( infile >> command )
    {
        if ( command == "tile_end" )
        {
            // push new tile
            geometryList.push_back( tile );
            // reset tile attributes
            tile.Init();
        }
        else if ( command == "orientation" )
        {
            // xz, xy, yz
        }
        // Location on tileset
        else if ( command == "filmstrip_x" )
        {
            infile >> value;    tile.FilmstripX( value );
        }
        else if ( command == "filmstrip_y" )
        {
            infile >> value;    tile.FilmstripY( value );
        }
        // In-game location
        else if ( command == "coord_x" )
        {
            infile >> value;    tile.X( value );
        }
        else if ( command == "coord_y" )
        {
            infile >> value;    tile.Y( value );
        }
        else if ( command == "coord_z" )
        {
            infile >> value;    tile.Z( value );
        }
    }

    return true;
}

}
