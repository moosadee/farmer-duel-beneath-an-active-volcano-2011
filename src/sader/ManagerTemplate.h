/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_ManagerTemplate
#define _Sader_ManagerTemplate

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include <string>
#include <iostream>
#include <vector>
#include <list>

#include "Enumerations.h"

namespace sader
{

template <class ITEM>

class ManagerTemplate
{
    protected:
        std::vector<ITEM> m_asset;
        int m_assetCount;
    public:
        ManagerTemplate()
        {
            Init();
        }

        void Init()
        {
            m_assetCount = 0;
        }

        int AssetCount() { return m_assetCount; }

        ~ManagerTemplate() { };

        // Removed when an m_asset list added
        void LoadAssetTemp()
        {
            ITEM temp;
            std::string filepath;

            // Load image
            filepath = "assets/textures/brick.png";
            if ( temp.Load( filepath ) ) { temp.name = "brick"; m_asset.Push( temp ); }
        }

        void LoadAssets( std::list<std::string> assetList )
        {
            m_assetCount = assetList.size();

            for ( std::list<std::string>::const_iterator filepath = assetList.begin();
                    filepath != assetList.end(); ++filepath )
            {
                ITEM temp;
                temp.m_name = (*filepath).substr( 0, (*filepath).size() - 4 ); // assuming file extention no longer than 3 chars

                if ( temp.Load( (std::string&)(*filepath) ) )
                {
                    m_asset.push_back( temp );
                }
            }
        }

        void DestroyAssets() { };

        int FindAsset( std::string name )
        {
            for ( unsigned int i=0; i<m_asset.size(); i++ )
            {
                if ( m_asset[i].m_name == name )
                  return i;
            }

            return -1;
        }

        ITEM& GetAsset( std::string name )
        {
            unsigned int index = FindAsset( name );

            if ( index < m_asset.size() && index >= 0 );
                return m_asset[index];
        }

        ITEM& GetAsset( unsigned int index )
        {
            if ( index < m_asset.size() && index >= 0 );
                return m_asset[index];
        }
};

}

#endif
