/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/

#ifndef _Sader_Asset
#define _Sader_Asset

namespace sader
{
    class Asset
    {
        private:
        public:
            std::string m_name;

            virtual bool Load( std::string& filepath ) = 0;
    };
}

#endif
