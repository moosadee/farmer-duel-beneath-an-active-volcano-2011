/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_KeyActionEnums
#define _Sader_KeyActionEnums

// This doesn't belong in sader; should go in the game itself.

    // Will have to be modified more once a menu is added
    const int TOTAL_KEY_ACTIONS = 23;

    enum KeyAction {    NOTHING = 0,
                        TOGGLE_FULLSCREEN = 1,
                        PLAYER_1_MOVE_UP = 2,
                        PLAYER_1_MOVE_DOWN = 3,
                        PLAYER_1_ATTACK = 4,

                        PLAYER_2_MOVE_UP = 5,
                        PLAYER_2_MOVE_DOWN = 6,
                        PLAYER_2_ATTACK = 7,

                        STATE_MENU = 8,
                        STATE_GAME = 9,
                        STATE_EXIT = 10,

                        CAMERA_X_PLUS = 11,
                        CAMERA_X_MINUS = 12,
                        CAMERA_Y_PLUS = 13,
                        CAMERA_Y_MINUS = 14,
                        CAMERA_Z_PLUS = 15,
                        CAMERA_Z_MINUS = 16,

                        CAMERA_X_ROT_PLUS = 17,
                        CAMERA_X_ROT_MINUS = 18,
                        CAMERA_Y_ROT_PLUS = 19,
                        CAMERA_Y_ROT_MINUS = 20,
                        CAMERA_Z_ROT_PLUS = 21,
                        CAMERA_Z_ROT_MINUS = 22
                    };

#endif
