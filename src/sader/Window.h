/*
Sader - Custom game framework
(c) Rachel J. Morris, 2011
http://www.moosader.com/

Sader - MIT
*/
#ifndef _Window
#define _Window

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

namespace sader
{
    // Wrapper for SFML's render window
    class Window
    {
        protected:
        public:
            sf::Window window;
            sf::WindowSettings settings;
            sf::Event event;
            void Init();
            void ToggleFullscreen();
            void Quit();
            void PrepScreen();
            void RenderScreen();
            bool IsRunning();
            void GetEvent();
    };
}

#endif
