/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Window.h"

namespace sader
{

/**
* A wrapper for SFML's "Close()" function.
* Closes the program.
*/
void Window::Quit()
{
  window.Close();
}

/**
* A wrapper for SFML's "Clear()" function.
* Clears the screen to prepare for next cycle's drawing.
*/
// TODO: Remove
void Window::PrepScreen()
{
  window.SetActive();
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

/**
* A wrapper for SFML's "Display()" function.
* Draws to screen.
*/
// TODO: Remove
void Window::RenderScreen()
{
  window.Display();
}

/**
* A wrapper for SFML's RenderWindow "IsOpened()" function.
*/
bool Window::IsRunning()
{
  return window.IsOpened();
}
void Window::GetEvent()
{
    while ( window.GetEvent( event ) )
    {
        // Close window : exit
        if ( event.Type == sf::Event::Closed )
        {
            window.Close();
        }

        // Escape key : exit
        if ( ( event.Type == sf::Event::KeyPressed ) && ( event.Key.Code == sf::Key::Escape ) )
        {
            window.Close();
        }

        // Resize event : adjust viewport
        if ( event.Type == sf::Event::Resized )
        {
            glViewport( 0, 0, event.Size.Width, event.Size.Height );
        }
    }
}

}
