/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_MapManager
#define _Sader_MapManager

#include "ManagerTemplate.h"
#include "Map.h"
#include "Texture.h"

namespace sader
{

class MapManager : public ManagerTemplate<Map>
{
    public:
        void LoadAssets( std::list<std::string> assetList, Texture& texture );
    private:
};

}

#endif
