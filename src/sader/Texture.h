/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_Texture
#define _Sader_Texture

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>
#include <iostream>

#include "Asset.h"
#include "ManagerTemplate.h"

namespace sader
{

class Texture : public Asset
{
    public:
        GLuint m_texture;
        float m_texture_w, m_texture_h;

        bool Load( std::string& filename );
};

}

#endif
