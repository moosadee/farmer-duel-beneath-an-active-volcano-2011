/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/

#include "Map.h"
#include "Texture.h"
#include "Camera.h"
#include "BaseCharacter.h"
#include "ManagerTemplate.h"

#ifndef _Sader_State
#define _Sader_State

namespace sader
{
    class State
    {
        protected:
            std::string name;
        public:
            virtual void Init() =0;
            virtual int MainLoop( Window* app ) =0;
            virtual void TestReport() =0;
    };
}

#endif
