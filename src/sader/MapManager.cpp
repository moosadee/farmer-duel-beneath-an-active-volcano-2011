/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "MapManager.h"

namespace sader
{

void MapManager::LoadAssets( std::list<std::string> assetList, Texture& texture )
{
    // Use parent class' functionality to load in the assets.
    ManagerTemplate<Map>::LoadAssets( assetList );

    // Set their texture to the level tileset
    // ToDo: Make this more general later.
    for ( std::vector<Map>::iterator map = m_asset.begin();
            map != m_asset.end(); ++map )
    {
        map->SetGeometryTexture( texture );
    }
}

}
