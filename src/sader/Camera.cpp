/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Camera.h"

namespace sader
{

Camera::Camera()
{
    m_RotationSpeed = 5.0f;
    m_TransformSpeed = 5.0f;
    m_xRotation = m_yRotation = m_zRotation = 0.0f;

    m_xTransform = 0.0f;
    m_yTransform = 0.0f;
    m_zTransform = -100.0f;
}

void Camera::Init( float mx, float my, float mz, float rx, float ry, float rz, float s, float rs )
{
    m_xTransform = mx;
    m_yTransform = my;
    m_zTransform = mz;

    m_xRotation = rx;
    m_yRotation = ry;
    m_zRotation = rz;

    m_TransformSpeed = s;
    m_RotationSpeed = rs;
}

/*
ToDo: Refactor w/ new Input class
void Camera::HandleInput( System& system )
{
    // Rotation
    if ( system.GetKey( sf::Key::Numpad2 ) ) { xRotation += speed; }
    if ( system.GetKey( sf::Key::Numpad8 ) ) { xRotation -= speed; }
    if ( system.GetKey( sf::Key::Numpad4 ) ) { yRotation += speed; }
    if ( system.GetKey( sf::Key::Numpad6 ) ) { yRotation -= speed; }
    if ( system.GetKey( sf::Key::Numpad7 ) ) { zRotation += speed; }
    if ( system.GetKey( sf::Key::Numpad3 ) ) { zRotation -= speed; }
    // Movement
    if ( system.GetKey( sf::Key::Numpad4 ) ) { xTransform += speed; }
    if ( system.GetKey( sf::Key::Numpad6 ) ) { xTransform -= speed; }
    if ( system.GetKey( sf::Key::Numpad8 ) ) { yTransform -= speed; }
    if ( system.GetKey( sf::Key::Numpad2 ) ) { yTransform += speed; }
    if ( system.GetKey( sf::Key::Equal ) ) { zTransform += speed; }
    if ( system.GetKey( sf::Key::Dash ) ) { zTransform -= speed; }
    // Reset rotation
    if ( system.GetKey( sf::Key::Return ) )
    {
        xRotation = yRotation = zRotation = 0;
    }
    if ( system.GetKey( sf::Key::Back ) )
    {
        std::cout<<"X: "<<xTransform<<", Y: " <<yTransform<<", Z: "<<zTransform<<std::endl;
    }
}
*/

void Camera::Prep()
{
      // Transformation
      glMatrixMode( GL_MODELVIEW );
      glLoadIdentity();
      glTranslatef( m_xTransform, m_yTransform, m_zTransform );
      glRotatef( m_xRotation, 1.0f, 0.0f, 0.0f );
      glRotatef( m_yRotation, 0.0f, 1.0f, 0.0f );
      glRotatef( m_zRotation, 0.0f, 0.0f, 1.0f );
}

void Camera::CameraToPoint( float x, float y )
{
    CameraToPoint( x, y, m_zTransform );
}

void Camera::CameraToPoint( float x, float y, float z )
{
    m_xTransform = -x;
    m_yTransform = -(y + 32);
    m_zTransform = z;
}

void Camera::RotX( int multiplier )
{
    m_xRotation += multiplier*m_RotationSpeed;
}

void Camera::RotY( int multiplier )
{
    m_yRotation += multiplier*m_RotationSpeed;
}

void Camera::RotZ( int multiplier )
{
    m_zRotation += multiplier*m_RotationSpeed;
}

void Camera::TransX( int multiplier )
{
    m_xTransform += multiplier*m_TransformSpeed;
}

void Camera::TransY( int multiplier )
{
    m_yTransform += multiplier*m_TransformSpeed;
}

void Camera::TransZ( int multiplier )
{
    m_zTransform += multiplier*m_TransformSpeed;
}


}
