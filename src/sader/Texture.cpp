/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Texture.h"

namespace sader
{

bool Texture::Load( std::string& filename )
{
    // Get filename
    for ( unsigned int i=0; i<filename.size(); i++ )
    {
        if ( filename[i] == '.' )
        {
            m_name = filename.substr( 0, i );
            break;
        }
    }

    // Load file
    std::string filepath = "content/graphics/" + filename;
    std::cout<<"Load texture "<<filepath<<"...";
    sf::Image image;
    if ( image.LoadFromFile(filepath) )
    {
        glGenTextures( 1, &m_texture );
        glBindTexture( GL_TEXTURE_2D, m_texture );
        gluBuild2DMipmaps( GL_TEXTURE_2D,
                            GL_RGBA,
                            image.GetWidth(),
                            image.GetHeight(),
                            GL_RGBA,
                            GL_UNSIGNED_BYTE,
                            image.GetPixelsPtr() );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
        m_texture_w = image.GetWidth();
        m_texture_h = image.GetHeight();

        std::cout<<"Success!"<<std::endl;
        return true;
    }

    std::cout<<"Failed!"<<std::endl;
    return false;
}

}

