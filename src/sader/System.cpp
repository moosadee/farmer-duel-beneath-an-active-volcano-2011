/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "System.h"

namespace sader
{

System::System()
{
    Init();
}

void System::Init()
{
    m_screenWidth = 1024;
    m_screenHeight = 768;
    m_bpp = 32;
    m_fullscreen = false;
    app.settings.DepthBits = 24;
    app.settings.StencilBits = 8;
    app.settings.AntialiasingLevel = 2;

    app.window.Create( sf::VideoMode( m_screenWidth, m_screenHeight, m_bpp ), "Sader", sf::Style::Close, app.settings );
    app.window.SetFramerateLimit(60);

    // Color/Depth
    glClearDepth( 1.0f );
    glClearColor( 0.5f, 0.5f, 0.5f, 0.5f );

    // Z-buffer read/write
    glEnable( GL_DEPTH_TEST );
    glDepthMask( GL_TRUE );
    glAlphaFunc(GL_GREATER, 0.5f);
    glEnable(GL_ALPHA_TEST);

    // Perspective projection
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective( 90.0f, 1.0f, 1.0f, 1500.0f );
}

/**
* Returns program m_status, such as 0 = ITSALLGOOD for no errors.
* @return   ProgramStatus   Returns an enumerated value for the state of the program.
*/
ProgramStatus System::GetStatus()
{
  return m_status;
}

}

