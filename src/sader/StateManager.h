/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _Sader_StateManager
#define _Sader_StateManager

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <vector>
#include <iostream>

#include "State.h"

namespace sader
{
    class StateManager
    {
        private:
            std::vector<State*> m_states;
            sader::Window* m_app;
            int m_activeStateIndex;
        public:
            void Init();
            void InitStates();
            void SetActiveState( int state );
            void Cleanup();
            int AddState( State* newState );
            int MainLoop();
            void SetRenderWindow( sader::Window* app );
    };
}

#endif
