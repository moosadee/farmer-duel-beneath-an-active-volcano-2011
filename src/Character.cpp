/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#include "Character.h"

void Character::Init( int playerNumber )
{
    moveSpeed = 8.0f;
    m_score = 0;

    m_film.x = 0;
    m_film.y = ( playerNumber == 1 ) ? 96 : 0;
    m_film.z = 0;
    m_film.w = 32;
    m_film.h = 48;
    m_film.d = 0;

    m_coord.x = 320;
    m_coord.y = 16;
    m_coord.z = ( playerNumber == 1 ) ? 32 : 480;
    m_coord.w = 10;
    m_coord.h = 108;
    m_coord.d = 72;

    m_collision.w = 32;
    m_collision.h = 84;
    m_collision.d = 50;
    m_collision.x = -16;//( playerNumber == 1 ) ? 16 : -16;
    m_collision.y = 0;
    m_collision.z = 8;
}

void Character::Move( Direction dir )
{
    if ( dir == UP )
    {
        if ( m_coord.x + moveSpeed < 610 )
        {
            m_coord.x += moveSpeed;
        }
    }
    else if ( dir == DOWN )
    {
        if ( m_coord.x - moveSpeed > 10 )
        {
            m_coord.x -= moveSpeed;
        }
    }
}




