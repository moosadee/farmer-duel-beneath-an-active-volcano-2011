/*
Farmer Duel beneath an Active Volcano
(c) Rachel J. Morris, 2011
June - July 2011 project
http://www.moosader.com/

Farmer Duel - GNU GPL v3
Sader - MIT
*/
#ifndef _State
#define _State

#include <iostream>

#include "sader/State.h"
#include "sader/Input.h"
#include "sader/Texture.h"
#include "sader/MapManager.h"
#include "sader/Geometry.h"
#include "sader/KeyActionEnums.h"

#include "Character.h"
#include "Item.h"
#include "Hud.h"

enum StateCode { EXIT=0, MENU=1, GAME=2 };

class MenuState : public sader::State
{
    private:
        StateCode m_code;
        sader::Input input;
        sader::ManagerTemplate<sader::Texture> textureManager;
        bool closeState;
        void TempRender( sader::Window* app );

        Hud titleScreen;
    public:
        void LoadAssets();
        void Init();
        void TestReport() { std::cout<<"Menu State test output"<<std::endl; }
        // Main game loop
        int MainLoop( sader::Window* app );
        // Accessors
        StateCode Code() { return m_code; }
};

// ToDo: Clean up!
class GameState : public sader::State
{
    private:
        bool debug;
        StateCode m_code;
        sader::Input input;
        sader::ManagerTemplate<sader::Texture> textureManager;
        sader::MapManager mapManager;

        int m_playerWins;
        // temp
        Character player1;
        Character player2;
        Item pong;
        Hud player1Score;
        Hud player2Score;
        Hud labelWins;
        Hud labelPlayer;
        Hud labelNumber;

        sader::BaseCharacter player1Boundary, player2Boundary; // boundary region for map
        bool closeState;
        bool pause;
        void HandleInput();
        void TempRender( sader::Window* app );
        sader::Camera cam;
    public:
        void Init();
        void TestReport() { std::cout<<"Game State test output"<<std::endl; }
        // Main game loop
        int MainLoop( sader::Window* app );
        void LoadAssets();
        // Accessors
        StateCode Code() { return m_code; }
};

#endif
