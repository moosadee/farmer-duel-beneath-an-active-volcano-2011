
FARMER 
  DUEL 
     beneath an 
        ACTIVE 
            VOLCANO

Rachel J. Morris / Moosader.com, 2011


*****************
* A  B  O  U  T *
*****************

FARMER DUEL beneath an ACTIVE VOLCANO was made for the
Moosader.com message board competition...
... LIMITED ASSETS !

The point of the competition was to use only Open-Licensed 
music and art assets.


**************************
* C  O  N  T  R  O  L  S *
**************************

From the TITLE SCREEN:
* Hit "F4" to quit
* Hit "Enter" to start the game.

From the GAME SCREEN:
* Hit "F4" to go back to the title (Warning! This will reset the game!)
* Hit "Enter" to begin the game when paused

* W and S move Player 1 UP and DOWN
* Up and Down move Player 2 UP and DOWN


***************************
* G  A  M  E - P  L  A  Y *
***************************

Currently, this is pong.
Hit the grenade to the other player's side, and try to get it off their side of the screen.

You must be leading by 4 to win.


***********************
* C  R  E  D  I  T  S *
***********************

Rachel J. Morris (Moosader)
* Programming
* Title screen & HUD text art
* Design? :P


Phillip Kilgore (Sir_Fawnpug)
* Farmer artwork
* Grenade artwork


Jacob Dix (cloudncali)
* Terrain artwork

